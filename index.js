// Bài 1: Quản lý tuyển sinh
// gắn sự kiện khi click vào kết quả
document.getElementById("btn-ket-qua").onclick = function () {
  //lấy thông tin input
  var diemToan = document.getElementById("txt-toan").value * 1;
  var diemLy = document.getElementById("txt-ly").value * 1;
  var diemHoa = document.getElementById("txt-hoa").value * 1;
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var khuVuc = document.getElementById("txt-khu-vuc").value;
  var doiTuong = document.getElementById("txt-doi-tuong").value;
  //   tính tổng điểm 3 môn thi
  var diemThi = tinhToan(diemToan, diemLy, diemHoa);
  console.log("diemThi: ", diemThi);
  //   tính tổng điểm khi xét tuyển sinh
  var tongDiem = 0;
  if (doiTuong == "1" && khuVuc == "A") {
    tongDiem = diemThi + 4.5;
  } else if (doiTuong == "1" && khuVuc == "B") {
    tongDiem = diemThi + 3.5;
  } else if (doiTuong == "1" && khuVuc == "C") {
    tongDiem = diemThi + 3;
  } else if (doiTuong == "2" && khuVuc == "A") {
    tongDiem = diemThi + 3.5;
  } else if (doiTuong == "2" && khuVuc == "B") {
    tongDiem = diemThi + 2.5;
  } else if (doiTuong == "2" && khuVuc == "C") {
    tongDiem = diemThi + 2;
  } else if (doiTuong == "3" && khuVuc == "A") {
    tongDiem = diemThi + 3;
  } else if (doiTuong == "3" && khuVuc == "B") {
    tongDiem = diemThi + 2;
  } else {
    tongDiem = diemThi + 1.5;
  }
  //   console.log("tongDiem: ", tongDiem);
  // xét tổng điểm đạt được so với điểm chuẩn, tạo biến chứa kết quả thông báo ra giao diện
  var thongBao = "";
  if (tongDiem >= diemChuan && diemToan >= 0 && diemLy >= 0 && diemHoa >= 0) {
    thongBao = "Bạn đã đậu vói tổng điểm là : " + tongDiem;
  } else {
    thongBao = "Bạn đã rớt vói tổng điểm là : " + tongDiem;
  }
  //   show kết quả lên giao diện
  document.getElementById("result1").innerHTML = thongBao;
};

// hàm tính tổng điểm 3 môn thi
function tinhToan(diem1, diem2, diem3) {
  var diem = 0;
  diem = diem1 + diem2 + diem3;
  return diem;
}

// Bài 2: Tính tiền điện
// gắn sự kiện khi click vào tính
document.getElementById("btn-tinh").onclick = function () {
  // lấy thông tin ô input
  var soKw = document.getElementById("txt-kw").value * 1;
  // tính tiền tiêu thụ điện qua từng trường hợp, tạo biến chứa tiền điện tính được
  var tienDien = 0;
  if (soKw >= 1 && soKw <= 50) {
    tienDien = soKw * 500;
  } else if (soKw > 50 && soKw <= 100) {
    tienDien = soKw * 650;
  } else if (soKw > 100 && soKw <= 200) {
    tienDien = soKw * 850;
  } else if (soKw > 200 && soKw <= 350) {
    tienDien = soKw * 1100;
  } else {
    tienDien = soKw * 1300;
  }
  //   show kết quả lên giao diện
  document.getElementById(
    "result2"
  ).innerText = `Tiền điện bạn phải đóng là ${tienDien.toLocaleString()} VND`;
};
